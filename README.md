<img src="readme/img/thumbnail.png" align="center" title="Murrengan network"/>

### Этот репозиторий содержит код серверной части приложения мурр_кард


#### Установка локально
```bash
# Убедиться, что активировано виртуальное окружение с python

git clone https://gitlab.com/murr_card/murr_back.git # копировать проект локально
pip install -r requirements.txt  # установка зависимостей python
python manage.py migrate # миграция базы данных
python manage.py runserver
```


Запустить бд в кубере
```bash
kubectl apply -f manifest/murr-pg.yaml
```

пробросить порт в постгрес

```bash
kubectl port-forward service/postgres 5432:5432
```

docker buildx build --platform=linux/amd64 -t registry.gitlab.com/murr_card/murr_back:0.3.0 . .



https://91.185.95.48/murr_back/api/murr_card/