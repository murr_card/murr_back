from django.db import models


class MurrCard(models.Model):
    title = models.CharField(max_length=224)
    content = models.TextField(blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
