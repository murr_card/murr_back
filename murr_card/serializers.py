from rest_framework import serializers

from .models import MurrCard


class MurrCardSerializers(serializers.ModelSerializer):
    class Meta:
        model = MurrCard
        fields = (
            "id",
            "title",
            "content",
            "timestamp",
        )

        read_only_fields = ("id", "timestamp")
