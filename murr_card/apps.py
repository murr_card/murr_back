from django.apps import AppConfig


class MurrCardConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "murr_card"
