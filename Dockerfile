FROM python:3.9-slim as builder
ENV PYTHONUNBUFFERED=1

RUN pip install -U pip setuptools wheel

WORKDIR /wheels
COPY requirements.txt /requirements.txt
RUN pip wheel -r /requirements.txt


FROM python:3.9-slim
ENV PYTHONUNBUFFERED=1

COPY --from=builder /wheels /wheels

RUN pip install -U pip setuptools wheel \
      && pip install /wheels/* \
      && rm -rf /wheels \
      && rm -rf /root/.cache/pip/*
RUN pip install gunicorn
WORKDIR /murrengan/home
COPY . .

EXPOSE 8000
ENV PYTHONPATH /murrengan/home



CMD ["gunicorn", "-c", "murr_back/gunicorn.py", "murr_back.wsgi:application"]