from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from rest_framework import routers
from django.contrib import admin
from murr_card import views

router = routers.DefaultRouter()

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/murr_card/", include("murr_card.urls")),
    # path('', include(router.urls)),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
